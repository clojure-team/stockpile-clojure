Source: stockpile-clojure
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               maven-repo-helper,
               clojure,
               libcommons-lang3-java,
               libpuppetlabs-i18n-clojure,
               libtext-markdown-perl | markdown,
               default-jdk-headless
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/clojure-team/stockpile-clojure
Vcs-Git: https://salsa.debian.org/clojure-team/stockpile-clojure.git
Homepage: https://github.com/puppetlabs/stockpile

Package: libstockpile-clojure
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends}
Recommends: ${java:Recommends}
Description: Simple, durable Java queuing library
 A simple, durable Java queueing library. Stockpile supports the durable
 storage and retrieval of data. After storage, stockpile returns an "entry"
 that can be used to access the data later, and when no longer needed, the data
 can be atomically discarded.
